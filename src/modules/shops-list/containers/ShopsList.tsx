import React, { Component } from 'react';
import { fetchShops } from '../../../apiCall';
import { Shop } from '../types';
import { ShopItem } from '../components/ShopItem/ShopItem';
import './ShopsList.scss';
import { Spinner } from '../components/Spinner';
import { ToastError } from '../components/ToastError';

export type ShopsListProps = {};

export type ShopsListState = {
  shopsList: Shop[];
  isLoadingShops: boolean;
  error: string;
};
export default class ShopsList extends Component<ShopsListProps, ShopsListState> {
  constructor(props: any) {
    super(props);
    this.state = {
      shopsList: [],
      isLoadingShops: false,
      error: '',
    };
  }

  componentDidMount() {
    const shops = fetchShops();
    this.setState(
      {
        isLoadingShops: true,
      },
      () => {
        shops
          .then(response => {
            setTimeout(
              () =>
                this.setState({
                  shopsList: response.results,
                  isLoadingShops: false,
                }),
              2000
            );
          })
          .catch(error => {
            this.setState({
              error,
              isLoadingShops: false,
            });
          });
      }
    );
  }

  render() {
    const { shopsList, isLoadingShops, error } = this.state;
    return (
      <div className="shopListcontainer">
        {!isLoadingShops ? (
          shopsList.map(shop => (
            <div key={shop.id} className="shopItemContainer">
              <ShopItem shop={shop} />
            </div>
          ))
        ) : (
          <Spinner />
        )}
        { error && <ToastError text="Problem encountered when loading shops!" />}
      </div>
    );
  }
}
