import React from 'react';

import './ToastError.scss';

export type ToastErrorProps = {
  text: string;
};
export const ToastError = (props: ToastErrorProps) => (
  <div className="toast">
    <div className="toastText">{props.text}</div>
  </div>
);
