import React, { Fragment } from 'react';

export type ImageProps = {
  src: string;
};

export const borderStyle = {
  border: '1px solid #c4c4c4',
};

export const Image = ({ src }: ImageProps) => (
  <Fragment>
    <img src={src} width="280" height="240" style={borderStyle} />
  </Fragment>
);
