import React from 'react';
import './InfoSpan.scss';

export type InfoSpanProps = {
  info: string;
  type: 'name' | 'address' | 'maxOffered';
};
export const InfoSpan = ({ info, type = 'name' }: InfoSpanProps) => {
  return <span className={type}>{info}</span>;
};
