import React, { Fragment } from 'react';
import { Shop } from '../../types';
import { Image } from '../Image';
import './ShopItem.scss';
import { InfoSpan } from '../InfoSpan';

export type ShopProps = {
  shop: Shop;
};
export const ShopItem = ({ shop }: ShopProps) => (
  <Fragment>
    <div>
      <Image src={shop.logo} />
    </div>
    <div className="infosContainer">
      <InfoSpan info={shop.name} type="name" />
      <InfoSpan info={shop.address} type="address" />
      <InfoSpan info={`${shop.zipcode} ${shop.city}`} type="address" />
      <InfoSpan info={`Up to ${shop.maxoffer} ${shop.currency} offered`} type="maxOffered" />
    </div>
  </Fragment>
);
