export type Shop = {
  id: number;
  shop_id: number;
  latitude: string;
  longitude: string;
  distance: string;
  name: string;
  chain: string;
  address: string;
  zipcode: string;
  city: string;
  category_id: number;
  category_name: string;
  logo: string;
  cover: string;
  maxoffer: string;
  currency: string;
};
