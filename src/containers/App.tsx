import React, { Component } from 'react';
import './App.scss';
import ShopsList from '../modules/shops-list/containers/ShopsList';

class App extends Component {
  render() {
    return (
      <div>
        <ShopsList />
      </div>
    );
  }
}

export default App;
