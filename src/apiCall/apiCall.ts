const createOptions = () => {
  const headers: HeadersInit = { 'Content-Type': 'application/json' };
  const options: RequestInit = { headers, method: 'GET' };
  return options;
};

const checkStatus = (response: Response) => {
  if (response.status >= 400) {
    return response
      .json()
      .catch(() => response)
      .then(() => {
        throw new Error('API ERROR');
      });
  }
  return Promise.resolve(response);
};

export const fetchShops = () => {
  return fetch('https://www.leshabitues.fr/testapi/shops', createOptions())
    .then(checkStatus)
    .then(response => {
      // try to parse the response body to JSON. If it fails, just return the response as is
      return response.json().catch(() => response);
    });
};
